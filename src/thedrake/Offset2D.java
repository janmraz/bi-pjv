package thedrake;

/**
 * Representing
 */
public class Offset2D {
    public final int x;
    public final int y;

    public Offset2D(int x, int y){
        this.x = x;
        this.y = y;
    }

    /**
     * Checks if provided numbers are equal
     *
     * @param x position number
     * @param y position number
     * @return true, the values are equal false otherwise
     */
    public boolean equalsTo(int x, int y) {
        return this.x == x && this.y == y;
    }

    /**
     * Returns new instance of {@link Offset2D} with inverted y value
     *
     * @return instance of {@link Offset2D}
     */
    public Offset2D yFlipped(){
        return new Offset2D(this.x,-this.y);
    }
}
