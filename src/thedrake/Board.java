package thedrake;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Board {
    private int dimension;
    private BoardTile[][] tiles;

    /**
     * Initiates the two dimensional array of board tiles
     *
     * @param dimension of array
     */
    public Board(int dimension) {
        this.dimension = dimension;
        this.tiles = new BoardTile[this.dimension][this.dimension];
        for(int i = 0; i< this.dimension;i++)
            for(int j = 0; j< this.dimension;j++)
                this.tiles[i][j] = BoardTile.EMPTY;
    }

    public Board(BoardTile[][] tiles, int dimension){
        this.tiles = tiles;
        this.dimension = dimension;
    }

    public int dimension() {
        return this.dimension;
    }

    /**
     * Returns {@link BoardTile} on given {@link BoardPos board position}
     *
     * @param pos position on board
     * @return board tile
     */
    public BoardTile at(BoardPos pos) {
        return this.tiles[pos.i()][pos.j()];
    }

    // Vytváří novou hrací desku s novými dlaždicemi. Všechny ostatní dlaždice zůstávají stejné
    public Board withTiles(TileAt ...ats) {
        BoardTile[][] copy = this.cloneTiles();
        for(TileAt at : ats){
            copy[at.pos.i()][at.pos.j()] = at.tile;
        }
        return new Board(copy, this.dimension);
    }

    private BoardTile[][] cloneTiles(){
        BoardTile [][] newArr = new BoardTile[this.dimension][this.dimension];
        for(int i = 0; i < this.tiles.length; i++)
            newArr[i] = this.tiles[i].clone();
        return newArr;
    }

    // Vytvoří instanci PositionFactory pro výrobu pozic na tomto hracím plánu
    public PositionFactory positionFactory() {
        return new PositionFactory(this.dimension);
    }

    public static class TileAt {
        public final BoardPos pos;
        public final BoardTile tile;

        public TileAt(BoardPos pos, BoardTile tile) {
            this.pos = pos;
            this.tile = tile;
        }
    }

    @Override
    public String toString(){
        String dimensionFieild = "\"dimension\":" + dimension();

        List<String> tilesFields = new ArrayList<>();
        for( int i = 0; i < dimension(); i++ )
            for(int j = 0; j < dimension(); j++ )
                tilesFields.add( "\"" + ( tiles[j][i] == BoardTile.MOUNTAIN ? "mountain" : "empty" ) + "\"");

        return dimensionFieild + ",\"tiles\":[" + String.join(",",tilesFields) + "]";
    }

}

