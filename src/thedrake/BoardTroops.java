package thedrake;

import java.util.*;
import java.util.stream.Collectors;

public class BoardTroops {
	private final PlayingSide playingSide;
	private final Map<BoardPos, TroopTile> troopMap;
	private final TilePos leaderPosition;
	private final int guards;


	
	public BoardTroops(PlayingSide playingSide) { 
		this.playingSide = playingSide;
		this.troopMap = Collections.emptyMap();
		this.leaderPosition = TilePos.OFF_BOARD;
		this.guards = 0;
	}
	
	public BoardTroops(
			PlayingSide playingSide,
			Map<BoardPos, TroopTile> troopMap,
			TilePos leaderPosition, 
			int guards) {
		this.playingSide = playingSide;
		this.troopMap = troopMap;
		this.leaderPosition = leaderPosition;
		this.guards = guards;
	}

	public Optional<TroopTile> at(TilePos pos) {
		TroopTile tile = this.troopMap.get(pos);
		if(tile == null)
			return Optional.empty();
		return Optional.of(tile);
	}
	
	public PlayingSide playingSide() {
		return this.playingSide;
	}
	
	public TilePos leaderPosition() {
		return this.leaderPosition;
	}

	public int guards() {
		return this.guards;
	}
	
	public boolean isLeaderPlaced() {
		return !this.leaderPosition.equals(TilePos.OFF_BOARD);
	}
	
	public boolean isPlacingGuards() {
		return leaderPosition != TilePos.OFF_BOARD && guards < 2;
	}
	
	public Set<BoardPos> troopPositions() {
		return this.troopMap.keySet();
	}

	public BoardTroops placeTroop(Troop troop, BoardPos target) {
		if(at(target).isPresent())
			throw new IllegalArgumentException();
		Map<BoardPos, TroopTile> newTroops =  new HashMap<>(this.troopMap);
		newTroops.put(target, new TroopTile(troop,this.playingSide,TroopFace.AVERS));
		return new BoardTroops(
				playingSide,
				newTroops,
				this.leaderPosition == TilePos.OFF_BOARD ? target : this.leaderPosition,
				isPlacingGuards() ? this.guards + 1 : this.guards);
	}
	
	public BoardTroops troopStep(BoardPos origin, BoardPos target) {
		if(!isLeaderPlaced()) {
			throw new IllegalStateException(
					"Cannot move troops before the leader is placed.");
		}

		if(isPlacingGuards()) {
			throw new IllegalStateException(
					"Cannot move troops before guards are placed.");
		}

		if(!at(origin).isPresent())
			throw new IllegalArgumentException();

		if(at(target).isPresent())
			throw new IllegalArgumentException();

		Map<BoardPos, TroopTile> newTroops = new HashMap<>(troopMap);
		TroopTile tile = newTroops.remove(origin);
		newTroops.put(target, tile.flipped());
		return new BoardTroops(
				playingSide,
				newTroops,
				leaderPosition.equals(origin) ? target : this.leaderPosition,
				this.guards);
	}
	
	public BoardTroops troopFlip(BoardPos origin) {
		if(!isLeaderPlaced()) {
			throw new IllegalStateException(
					"Cannot move troops before the leader is placed.");			
		}
		
		if(isPlacingGuards()) {
			throw new IllegalStateException(
					"Cannot move troops before guards are placed.");			
		}

		if(!at(origin).isPresent())
			throw new IllegalArgumentException();
		
		Map<BoardPos, TroopTile> newTroops = new HashMap<>(troopMap);
		TroopTile tile = newTroops.remove(origin);
		newTroops.put(origin, tile.flipped());

		return new BoardTroops(playingSide(), newTroops, leaderPosition, guards);
	}
	
	public BoardTroops removeTroop(BoardPos target) {
		if(!isLeaderPlaced()) {
			throw new IllegalStateException(
					"Cannot move troops before the leader is placed.");
		}

		if(isPlacingGuards()) {
			throw new IllegalStateException(
					"Cannot move troops before guards are placed.");
		}

		if(!at(target).isPresent())
			throw new IllegalArgumentException();

		Map<BoardPos, TroopTile> newTroops = new HashMap<>(troopMap);
		newTroops.remove(target);
		return new BoardTroops(
				this.playingSide,
				newTroops,
				leaderPosition.equals(target) ? TilePos.OFF_BOARD : this.leaderPosition,
				this.guards);
	}

	@Override
	public String toString() {
		Boolean i = false;
		String out = new String();
		String sideField = "\"side\":\"" + playingSide.name() + "\"";
		String leaderPositionField = "\"leaderPosition\":\"" + leaderPosition().toString() + "\"";
		String guardsField = "\"guards\":" + guards();
		StringBuilder troopField = new StringBuilder("\"troopMap\":{");
		Set<BoardPos> troopSet = troopMap.keySet();
		List<String> re = new ArrayList<>();
		for( BoardPos boardPos : troopSet ){
			String troop = new String();
			troop += "\"";
			troop += boardPos.toString();
			troop += "\":{";
			troop += troopMap.get(boardPos).toString();
			troop += "}";
			re.add(troop);
		}
		re.sort(String::compareTo);
		troopField.append(String.join(",",re));
		troopField.append("}");
		return sideField + "," + leaderPositionField + "," + guardsField + "," + troopField;
	}
}