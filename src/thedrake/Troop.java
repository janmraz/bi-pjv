package thedrake;

import java.util.List;

/**
 * Representing a unit in a game. It holds the averse pivot and reverse pivot
 */
public class Troop {
    private final String name;
    private final Offset2D aversPivot;
    private final Offset2D reversPivot;
    private final List<TroopAction> aversActions;
    private final List<TroopAction> reversActions;


    public Troop(String name, Offset2D aversPivot, Offset2D reversPivot, List<TroopAction> aversActions, List<TroopAction> reversActions){
        this.name = name;
        this.aversPivot = aversPivot;
        this.reversPivot = reversPivot;
        this.aversActions = aversActions;
        this.reversActions = reversActions;
    }

    public Troop(String name, Offset2D pivot, List<TroopAction> aversActions, List<TroopAction> reversActions) {
        this(name, new Offset2D(pivot.x,pivot.y), new Offset2D(pivot.x,pivot.y), aversActions, reversActions);
    }

    public Troop(String name, List<TroopAction> aversActions, List<TroopAction> reversActions){
        this(name, new Offset2D(1,1), new Offset2D(1,1), aversActions, reversActions);
    }

    /**
     * Returns the troop's name
     *
     * @return name of troop
     */
    public String name(){
        return this.name;
    }

    /**
     * Returns the troop's pivot based on given state (reverse or averse)
     *
     * @param face enum value representing the state
     * @return avers pivot, if the state is averse, otherwise reverse pivot
     */
    public Offset2D pivot(TroopFace face){
        return face == TroopFace.AVERS ? this.aversPivot : this.reversPivot;
    }


    public List<TroopAction> actions(TroopFace face){
        return face == TroopFace.AVERS ? aversActions : reversActions;
    }

}
