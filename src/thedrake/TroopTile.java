package thedrake;

import java.util.ArrayList;
import java.util.List;

public class TroopTile implements Tile {
    private Troop troop;
    private PlayingSide side;
    private TroopFace face;

    public TroopTile(Troop troop, PlayingSide side, TroopFace face){
        this.troop = troop;
        this.side = side;
        this.face = face;
    }

    /**
     * Returns the color of side of troop on this position
     */
    public PlayingSide side() {
        return side;
    }

    /**
     * Returns side on which is troop sided on
     */
    public TroopFace face(){
        return face;
    }

    /**
     * Returns troop standing on this tile
     */
    public Troop troop() {
        return troop;
    }

    /**
     * Returns new {@link TroopTile} with flipped side
     */
    public TroopTile flipped(){
        return new TroopTile(troop,side, face == TroopFace.AVERS ? TroopFace.REVERS : TroopFace.AVERS);
    }

    @Override
    public boolean canStepOn() {
        return !hasTroop();
    }

    @Override
    public boolean hasTroop() {
        return troop != null;
    }

    @Override
    public List<Move> movesFrom(BoardPos pos, GameState state) {
        List<Move> result = new ArrayList<Move>();
        if(pos == BoardPos.OFF_BOARD)
            return result;
        List<TroopAction> actions = troop().actions(face);

        for (TroopAction troopAction : actions)
            for(Move move : troopAction.movesFrom(pos, side, state))
                result.add(move);

        return result;

    }

    @Override
    public String toString() {
        return "\"troop\":\"" + troop.name() + "\"," +
                "\"side\":\"" + side + "\"," +
                "\"face\":\"" + face + "\"";
    }
}
