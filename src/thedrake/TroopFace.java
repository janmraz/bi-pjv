package thedrake;

/**
 * Enum representing orientation of card
 */
public enum TroopFace {
    AVERS, REVERS
}
