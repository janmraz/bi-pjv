package thedrake.ui;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import thedrake.GameState;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("The Drake");
        primaryStage.setScene(new Scene(root, 1600, 900));
        primaryStage.show();
    }

    @FXML
    public void end(){
        Platform.exit();
    }

    @FXML
    public void startGame(ActionEvent event) {
        System.out.println("Multi Player");

        TheDrakeApp application = new TheDrakeApp();

        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();

        try {
            application.start(stage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
