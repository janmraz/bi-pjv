package thedrake.ui;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import thedrake.Board;
import thedrake.BoardTile;
import thedrake.GameState;
import thedrake.PositionFactory;
import thedrake.StandardDrakeSetup;

public class TheDrakeApp extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        HBox root = new HBox();
        Scene scene = new Scene(root);

        VBox main = new VBox();
        root.getChildren().add(main);


        HBox capturedStats = new HBox();
        main.getChildren().add(capturedStats);
        main.setSpacing(5.0);
        capturedStats.setAlignment(Pos.TOP_CENTER);
        Label blueCaptured = new Label("0");
        blueCaptured.paddingProperty().setValue(new Insets(0, 10.0, 0, 0));
        capturedStats.getChildren().add(blueCaptured);
        Label orangeCaptured = new Label("0");
        orangeCaptured.paddingProperty().setValue(new Insets(0, 0, 0, 10.0));
        capturedStats.getChildren().add(orangeCaptured);

        GameState gameState = createSampleGameState();
        BoardView boardView = new BoardView(gameState, primaryStage, blueCaptured, orangeCaptured);
        main.getChildren().add(boardView);

        HBox actionSection = new HBox();
        actionSection.setSpacing(10.0);
        actionSection.setAlignment(Pos.TOP_CENTER);

        Button placeFromStack = new Button();
        placeFromStack.setText("Place From Stack");
        placeFromStack.setOnMouseClicked(event -> boardView.stackMoves());
        actionSection.getChildren().add(placeFromStack);

        Button resign = new Button();
        resign.setText("Resign");
        resign.setOnMouseClicked(event -> boardView.resign());
        actionSection.getChildren().add(resign);

        main.getChildren().add(actionSection);


        primaryStage.setScene(scene);
        primaryStage.setTitle("The Drake");
        primaryStage.show();
    }

    private static GameState createSampleGameState() {
        Board board = new Board(4);
        PositionFactory positionFactory = board.positionFactory();
        board = board.withTiles(new Board.TileAt(positionFactory.pos(1, 1), BoardTile.MOUNTAIN));
        return new StandardDrakeSetup().startState(board);
    }

}
