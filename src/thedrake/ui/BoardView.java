package thedrake.ui;

import java.util.List;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import thedrake.*;

import static thedrake.GameResult.VICTORY;

public class BoardView extends GridPane implements TileViewContext {

    private GameState gameState;

    private ValidMoves validMoves;

    private TileView selected;

    private Stage callbackStage;

    private Label blueCaptured;
    private Label orangeCaptured;

    public BoardView(GameState gameState, Stage callbackStage, Label blueCaptured, Label orangeCaptured) {
        this.gameState = gameState;
        this.validMoves = new ValidMoves(gameState);
        this.callbackStage = callbackStage;
        this.blueCaptured = blueCaptured;
        this.orangeCaptured = orangeCaptured;

        PositionFactory positionFactory = gameState.board().positionFactory();
        for (int y = 0; y < 4; y++) {
            for (int x = 0; x < 4; x++) {
                int i = x;
                int j = 3 - y;
                BoardPos boardPos = positionFactory.pos(i, j);
                add(new TileView(boardPos, gameState.tileAt(boardPos), this), x, y);
            }
        }

        setHgap(5);
        setVgap(5);
        setPadding(new Insets(15));
        setAlignment(Pos.CENTER);

        showMoves(this.validMoves.movesFromStack());

    }

    @Override
    public void tileViewSelected(TileView tileView) {
        if (selected != null && selected != tileView) {
            selected.unselect();
        }

        selected = tileView;

        clearMoves();
        showMoves(validMoves.boardMoves(tileView.position()));
    }

    @Override
    public void executeMove(Move move) {
        if(selected != null){
            selected.unselect();
        }

        clearMoves();
        gameState = move.execute(gameState);
        validMoves = new ValidMoves(gameState);

        blueCaptured.setText("Blue: " + gameState.army(PlayingSide.ORANGE).captured().size());
        orangeCaptured.setText("Orange: " + gameState.army(PlayingSide.BLUE).captured().size());



        boolean placingGuards = gameState.armyNotOnTurn().boardTroops().isPlacingGuards() || gameState.armyOnTurn().boardTroops().isPlacingGuards();
        if(placingGuards){
            showMoves(this.validMoves.movesFromStack());
        }

        if (gameState.result() == VICTORY) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Game over !");
            alert.setHeaderText((gameState.armyOnTurn().side() == PlayingSide.BLUE ? "Blue" : "Orange") + " won !");

            alert.showAndWait();
            try {
                new Main().start(callbackStage);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        selected = null;

        updateTiles();
    }

    public void stackMoves(){
        clearMoves();
        if(selected != null){
            selected.unselect();
        }
        selected = null;
        showMoves(this.validMoves.movesFromStack());
    }

    private void updateTiles() {
        for (Node node : getChildren()) {
            TileView tileView = (TileView) node;
            tileView.setTile(gameState.tileAt(tileView.position()));
            tileView.update();
        }
    }

    private void clearMoves() {
        for (Node node : getChildren()) {
            TileView tileView = (TileView) node;
            tileView.clearMove();
        }
    }

    private void showMoves(List<Move> moveList) {
        for (Move move : moveList) {
            tileViewAt(move.target()).setMove(move);
        }
    }

    private TileView tileViewAt(BoardPos target) {
        int index = (3 - target.j()) * 4 + target.i();
        return (TileView) getChildren().get(index);

    }

    public void resign() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Game over !");
        alert.setHeaderText((gameState.armyOnTurn().side() == PlayingSide.BLUE ? "Blue" : "Orange") + " resigned !");

        if (alert.showAndWait().get() == ButtonType.OK) {
            try {
                new Main().start(callbackStage);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
